Si quieres disfrutar de la fotografía ya seas aficionado o profesional y aprovechar todas las características de la cámara ya sea la del móvil o la cámara réflex necesitas un trípode.
Este accesorio es imprescindible para cualquier fotógrafo, podrás ver todo lo que puedes hacer con un trípode y encontrar el que estas buscando.  
Podras encontrar las mejores marcas en trípodes tanto para móviles como para cámaras réflex, del material que más se ajuste a tus necesidades.
Si quieres conseguir los mejores resultados necesitas un soporte que le de estabilidad a tu cámara y tus fotos no salgan movidas encuentra el que buscas en:
www.detripodes.com